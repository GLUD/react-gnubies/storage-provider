export default function ListItems(props) {
    return (
        <div class="list-items">
            <table>
                <thead>
                    <tr>
                        <th>Key</th>
                        <th>Value</th>
                        <th className="remove">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {props.listItems().map(([key, value]) => (
                        <tr key={key}>
                            <td>{key}</td>
                            <td>{value}</td>
                            <td className="remove" onClick={() => props.removeItem(key)}><h1>X</h1></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}