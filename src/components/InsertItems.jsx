import { Component } from "react";

export default class InsertItems extends Component {
    constructor(props) {
        super(props);

        this.state = {
            key: '',
            value: ''
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit() {
        this.props.setItem(this.state.key, this.state.value);
        this.setState({
            key: '',
            value: ''
        });
    }

    render() {
        return (
            <div class="insert-items">
                <label>
                    <p>Key:</p>
                    <input type="text" value={this.state.key} onChange={({target: {value}}) => {this.setState({key: value})}} />
                </label>
                <label>
                    <p>Value:</p>
                    <input type="text" value={this.state.value} onChange={({target: {value}}) => {this.setState({value: value})}} />
                </label>
                <button onClick={() => this.onSubmit()}>Submit</button>
            </div>
        );
    }

}