import { Component } from "react";

export default class StorageProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.methods = {
            setItem: this.setItem.bind(this),
            getItem: this.getItem.bind(this),
            removeItem: this.removeItem.bind(this),
            listItems: this.listItems.bind(this)
        };
    }

    setItem(key, value) {
        this.setState({
            [key]: value
        });
    }

    getItem(key) {
        return this.state[key];
    }

    removeItem(key) {
        delete this.state[key];
        this.setState(this.state);
    }

    listItems() {
        return Object.entries(this.state);
    }

    render() {
        return (
            <div class="provider">
                {this.props.children(this.methods)}
            </div>
        );
    }


}