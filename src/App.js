import './App.css';
import StorageProvider from './components/StorageProvider';
import InsertItems from './components/InsertItems';
import ListItems from './components/ListItems';

function App() {
  return (
    <StorageProvider>
      {
        (handlers) => (
          <>
            <InsertItems {...handlers} />
            <ListItems {...handlers} />
          </>
        )
      }
    </StorageProvider>
  );
}

export default App;
